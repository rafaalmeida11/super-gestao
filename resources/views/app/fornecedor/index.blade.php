<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href=""
      rel="stylesheet"
    />
    <title>Fornecedor</title>
  </head>
  <body>
    <h3>Fornecedor</h3>

    @php
        /*
        if(!<condicao>) {} //enquanto executa se o retorno for true

        if(isset($variavel)) {} // retornar true se a variável estiver definida

        if(empty($variavel)) {} // retornar true se a variável estiver vazia
        */
    @endphp

    {{-- @unless executa se o retorno for false --}}

    @isset($fornecedores)
        @forelse($fornecedores as $indice => $fornecedor)
            {{-- @dd($loop) --}}
            Iteração atual: {{ $loop->iteration }}
            <br>
            Fornecedor: {{ $fornecedor['nome'] }}
            <br>
            Status: {{ $fornecedor['status'] }}
            <br>
            CNPJ: {{ $fornecedor['cnpj'] ?? 'Dado não foi preenchido' }}
            <br>
            Telefone: ({{ $fornecedor['ddd'] ?? 'Dado não foi preenchido' }}) {{ $fornecedor['telefone'] ?? 'Dado não foi preenchido' }}
            <br>

            @if($loop->first)
                Primeira iteração do loop
            @endif

            @if($loop->last)
                Ultima iteração do loop

                <br>

                Total de registros: {{$loop->count}}
            @endif
            <hr>
            <br>
        @empty
            Não existem fornecedores cadastrados
        @endforelse
    @endisset

  </body>
</html>
