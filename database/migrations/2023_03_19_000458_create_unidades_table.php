<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->id();
            $table->string('unidade', 5); // cm, mm, kg
            $table->string('descricao');
            $table->timestamps();
        });

        // adicionar o relacionamento com a tabela produtos, podemos fazer direto por essa migration aqui, ao invés de criar outra migration
        Schema::table('produtos', function(Blueprint $table) {
            $table->unsignedBigInteger('unidade_id');
            $table->foreign('unidade_id')->references('id')->on('unidades'); // constraint de fore key
        });

        // adicionar o relacionamento com a tabela produto_detalhes

        Schema::table('produto_detalhes', function(Blueprint $table) {
            $table->unsignedBigInteger('unidade_id');
            $table->foreign('unidade_id')->references('id')->on('unidades'); // constraint de fore key
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // antes de excluir a tabela unidades, nós precisamos remover os relacionamentos
        Schema::table('produto_detalhes', function(Blueprint $table) {
            $table->dropForeign('produto_detalhes_unidade_id_foreign'); //[table]_[coluna]_foreign

            $table->dropColumn('unidade_id');
        });

        Schema::table('produtos', function(Blueprint $table) {
            $table->dropForeign('produtos_unidade_id_foreign'); //[table]_[coluna]_foreign

            $table->dropColumn('unidade_id');
        });

        Schema::dropIfExists('unidades');
    }
};
